<?php

namespace Drupal\modulo1\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class Modulo2Controller extends ControllerBase {

/**
 * {@inheritdoc}
 */
  public function build() {
    $build = [
      '#type' => 'markup',
      '#markup' => 'bye World!',
    ];

  return $build;

  }

}
