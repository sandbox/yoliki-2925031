<?php

namespace Drupal\modulo1\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class Modulo1Controller extends ControllerBase {

/**
 * {@inheritdoc}
 */
  public function build() {
    $build = [
      '#type' => 'markup',
      '#markup' => 'Hello World!',
    ];

  return $build;

  }

}
